#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import matplotlib.pyplot as plt
import math

def parse_args():
    parser = argparse.ArgumentParser(description='Generate random cities.')
    
    parser.add_argument('-c', dest='cities', type=str, required=True, help='File with cities.')
    parser.add_argument('-t', dest='tour', type=str, required=True, help='File with tour.')

    return parser.parse_args()

def distance(a, b):
    return math.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

def length(tour):
    return sum(distance(tour[i-1], tour[i]) for i in range(len(tour)))

def plot(tour, distance):
    fig = plt.figure(figsize=(10, 5))
    
    tour += [tour[0]]

    X, Y = zip(*tour)
    plt.plot(X, Y, 'o-')

    plt.plot(tour[0][0], tour[0][1], 'o', color='red', markersize=10)

    plt.axis('off')
    # plt.tight_layout()

    plt.title('DISTANCE: {0:.3f}'.format(distance))
    
    plt.show()

def load_cities(filename):
    cities = []

    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

    for city in lines:
        coords = city.split(' ')
        cities.append(tuple(map(float, coords)))
    
    return cities
    

def load_tour(filename):
    f = open(filename, 'r')
    line = f.read()
    f.close()

    tour = line.strip().split(' ')
    return float(tour[0]), map(int, tour[1:])
    

if __name__ == '__main__':
    args = parse_args()
    
    cities = load_cities(args.cities)
    _, tour = load_tour(args.tour)
    
    assert len(cities) == len(tour)
    visited = [0] * len(cities)
    for c in tour:
        visited[c] += 1

    assert all(v == 1 for v in visited)

    tour = [cities[i] for i in tour]
    plot(tour, length(tour))
