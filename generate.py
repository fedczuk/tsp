#!/usr/bin/env python
# -*- coding: utf-8 -*-

from random import seed, randint
import argparse

X_BOUNDS = (1, 2000)
Y_BOUNDS = (1, 1000)

def parse_args():
    parser = argparse.ArgumentParser(description='Generate random cities.')
    
    parser.add_argument('-s', dest='seed', type=int, default=321, help='Random number generator seed.')
    parser.add_argument('-n', dest='n', type=int, required=True, help='Number of cities.')

    return parser.parse_args()

def generate(n, s):
    seed(s)

    cities = set()
    while len(cities) < n:
        x, y = randint(*X_BOUNDS), randint(*Y_BOUNDS) 
        cities.add((x, y))

    return cities

def save(filename, cities):
    f = open(filename, 'w')
    for c in cities:
        f.write('{} {}\n'.format(c[0], c[1]))

    f.close()

if __name__ == '__main__':
    args = parse_args()
    
    cities = generate(args.n, args.seed)

    filename = 'tsp-{}-s{}.txt'.format(len(cities),  args.seed)
    save(filename, cities)

    print('CREATED: {}'.format(filename))


