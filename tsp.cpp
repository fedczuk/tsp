#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <random>
#include <utility>

/**
 * KOMPILACJA: g++ tsp.cpp -O3 -o tsp
 */

#define distance(a, b) dm[a][b]

using City = std::pair<double, double>;
using Move = std::pair<int, int>;
using Tour = std::vector<int>;

std::vector<std::vector<double>> dm;

/**
 * Ładuje miasta z pliku.
 *
 * @param filename Ścieżka do pliku ze współrzędnymi miast.
 * @return Lista miast.
 */
auto load_cities(const std::string &filename) {
    std::vector<City> cities;

    std::ifstream input(filename);
    if (input.is_open()) {
        double x, y;
        while (input >> x >> y) {
            cities.emplace_back(x, y);
        }

        input.close();
    }

    return cities;
}

/**
 * Wyznacza macierz dystansów korzystając z metryki euklidesowej.
 *
 * @param cities Lista miast.
 */
void calc_distance_matrix(std::vector<City> &cities) {
    dm.resize(cities.size());

    for (int i = 0; i < cities.size(); i++) {
        dm[i].resize(cities.size());

        for (int j = 0; j < cities.size(); j++) {
            // obliczamy odległość między dwoma miastami,
            // zgodnie ze wzorem na metrykę euklidesową
            double dx = cities[i].first - cities[j].first;
            double dy = cities[i].second - cities[j].second;
            dm[i][j] = sqrt(dx * dx + dy * dy);
        }
    }
}

/**
 * Układa trasę za pomocą algorytmu najbliższego sąsiada.
 *
 * @param start Numer miasta startowego.
 * @param cities Ilość wszystkich miast.
 * @return Zbudowana trasa.
 */
auto nn(int start, unsigned long cities) {
    Tour tour(cities);

    // inicjalizujemy trasę
    std::iota(tour.begin(), tour.end(), 0);

    // ustawiamy miasto startowe na początku trasy
    std::swap(tour[0], tour[start]);

    // budujemy trasę
    for (int i = 0; i < cities - 1; i++) {
        int nearest = i + 1;

        // szukamy najbliższego nieodwiedzonego sąsiada
        for (int j = nearest + 1; j < cities; j++) {
            if (distance(tour[i], tour[j]) < distance(tour[i], tour[nearest])) {
                nearest = j;
            }
        }

        // przestawiamy znalezione miasto
        // na następną pozycję w trasie
        std::swap(tour[i + 1], tour[nearest]);
    }

    return tour;
}

/**
 * Określa jaki fragment trasy należy odwrócić,
 * aby jak najbardziej zmniejszyć jej dystans.
 *
 * @param tour Trasa.
 * @return Indeksy pomiędzy którymi należy obrócić trasę.
 */
auto find_best_move(const Tour &tour) {
    Move move(-1, -1);

    double max_gain = 0;
    for (int i = 1; i < tour.size() - 1; i++) {
        // wyznaczamy miasta połączone przez pierwszą krawędź
        int a = tour[i - 1], b = tour[i];

        for (int j = i + 1; j < tour.size(); j++) {
            // wyznaczamy miasta połączone przez drugą krawędź
            int c = tour[j - 1], d = tour[j];

            // określamy o ile zmniejszy się trasa,
            // jeśli zastosujemy alternatywne połączenie miast
            double current = distance(a, b) + distance(c, d);
            double alternative = distance(a, c) + distance(b, d);
            double gain = current - alternative;

            // gdy znajdziemy lepszy ruch
            if (gain > max_gain) {
                // zapamiętujemy indeksy pomiędzy
                // którymi należy obrócić trasę
                max_gain = gain;
                move.first = i;
                move.second = j;
            }
        }
    }

    return move;
}

/**
 * Optymalizuje trasę za pomocą algorytmu 2-opt.
 *
 * @param tour Trasa.
 */
void two_opt(Tour &tour) {
    bool improved;

    do {
        improved = false;

        // spróbuj znaleźć ruch, który najbardziej skróci trasę
        auto move = find_best_move(tour);

        // jeśli taki ruch istnieje
        if (move.first >= 0) {
            // odwróć trasę pomiędzy wskazanymi punktami
            std::reverse(tour.begin() + move.first, tour.begin() + move.second);
            improved = true;
        }

        // przenosimy dwa miasta z początku na koniec trasy,
        // zachowując przy tym kolejność ich odwiedzania,
        // aby funkcja find_best_move, w kolejnym obiegu pętli,
        // mógła uwzględnić podczas analizy krawędź powrotną
        std::rotate(tour.begin(), tour.begin() + 2, tour.end());
    } while (improved);
}

/**
 * Wyznacza długość trasy.
 *
 * @param tour Trasa.
 * @return Długość trasy.
 */
double length(const Tour &tour) {
    double result = 0;

    for (int i = 0; i < tour.size(); i++) {
        result += distance(tour[i], tour[(i + 1) % tour.size()]);
    }

    return result;
}

/**
 * Wyświetla na ekranie trasę oraz jej długość.
 *
 * @param tour Trasa.
 */
void print(const Tour &tour) {
    std::cout << length(tour) << " ";
    for (auto city : tour) {
        std::cout << city << " ";
    }
    std::cout << std::endl;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "USAGE: ./tsp [TSP_BENCHMARK_FILE]" << std::endl;
        return 1;
    }

    std::string filename = argv[1];
    auto cities = load_cities(filename);

    calc_distance_matrix(cities);

    Tour best_tour(cities.size());
    std::iota(best_tour.begin(), best_tour.end(), 0);

    double best_tour_distance = length(best_tour);

    // inicjalizujemy generator liczb losowych
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> rand(0, static_cast<int>(cities.size() - 1));

    // określamy maksymalną liczbę iteracji
    int iterations = static_cast<int>(cities.size() * 0.2);

    for (int i = 0; i < iterations; i++) {
        // losujemy miasto startowe
        int start = rand(gen);

        auto tour = nn(start, cities.size());
        two_opt(tour);

        double dst = length(tour);

        if (dst < best_tour_distance) {
            best_tour = tour;
            best_tour_distance = dst;
        }
    }

    print(best_tour);

    return 0;
}
